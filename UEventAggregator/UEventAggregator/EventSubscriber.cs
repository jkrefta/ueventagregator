using System;

namespace UEventAggregator
{
    public class EventSubscriber
    {
        public Action<object> Action { get; }

        public EventSubscriber(Action<object> action)
        {
            Action = action;
        }

        public void Send(object message)
        {
            Action.Invoke(message);
        }
    }
}