﻿using System;
using System.Collections.Generic;

namespace UEventAggregator
{
    public class UEventAggregator
    {
        public Dictionary<string, List<EventSubscriber>> Subscribed;

        public UEventAggregator()
        {
            Subscribed = new Dictionary<string, List<EventSubscriber>>();
        }

        public void Subscribe(string subject, Action<object> action)
        {
            var eventSubscriber = new EventSubscriber(action);
            if (!Subscribed.ContainsKey(subject))
            {
                Subscribed.Add(subject, new List<EventSubscriber>());
            }
            Subscribed[subject].Add(eventSubscriber);
        }

        public void Publish(string subject, object message)
        {
            if (Subscribed.ContainsKey(subject))
            {
                Subscribed[subject].ForEach(subscriber => subscriber.Send(message));
            }
        }
    }
}
