﻿using System;
using NUnit.Framework;

namespace UEventAggregator.Tests
{
    [TestFixture]
    public class UEventAggregatorTests
    {
        private readonly Action<object> _noopAction = new Action<object>(Noop);
        private UEventAggregator _uEventAggregator;
        private const string GenericSubject = "Subject";

        [SetUp]
        public void SetUp()
        {
            _uEventAggregator = new UEventAggregator();
        }

        [Test]
        public void Subscribe_GivenSubjectAndHandler_ShouldAddEntryToSubscriberStore()
        {
            _uEventAggregator = new UEventAggregator();
            _uEventAggregator.Subscribe(GenericSubject, _noopAction);
            Assert.That(_uEventAggregator.Subscribed["Subject"].Count, Is.EqualTo(1));
        }

        [Test]
        public void Subscribe_GivenSubjectAndHandler_WhenOneSubscriberWasAlreadyRegistered_ShouldAddEntryToSubscriberStore()
        {
            _uEventAggregator.Subscribe(GenericSubject, _noopAction);
            _uEventAggregator.Subscribe(GenericSubject, _noopAction);
            Assert.That(_uEventAggregator.Subscribed[GenericSubject].Count, Is.EqualTo(2));
        }

        [Test]
        public void Publish_GivenSubjectAndMessage_ShouldExecuteAllSubscribedHandlers()
        {
            bool wasExecuted = false;
            _uEventAggregator.Subscribe(GenericSubject, (object message) => wasExecuted = (bool)message);
            _uEventAggregator.Publish(GenericSubject, true);
            Assert.That(wasExecuted, Is.True);
        }

        public static void Noop(object x)
        {
        }

    }
}
